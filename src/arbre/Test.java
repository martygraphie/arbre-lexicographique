package arbre;

public class Test {

	public static void main(String[] args) {
		ArbreLexicographique arbre = new ArbreLexicographique();
		System.out.println(arbre.ajout("FARE"));
		System.out.println(arbre.suppr(""));
		System.out.println(arbre.ajout("FAT"));
		System.out.println(arbre.ajout("FATE"));
		arbre.ajout("FATED");
		arbre.ajout("FOR");
		arbre.ajout("ICE");
		System.out.println(arbre.ajout("IT"));
		System.out.println(arbre.ajout("IT"));
		System.out.println("------------");
		System.out.print(arbre.toString());
		System.out.println("------------");
		System.out.println("Nombre de mots : " + arbre.nbMots());
		System.out.println("FAR appartient ? " + arbre.contient("FAR"));
		System.out.println("FAR prefixe ? " + arbre.prefixe("FAR"));
		System.out.println(arbre.suppr("ABSENT"));
		System.out.println(arbre.suppr("FOR"));
		System.out.println("------------");
		System.out.print(arbre.toString());
		System.out.println("------------");
	}

}
