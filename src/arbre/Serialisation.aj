package arbre;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public aspect Serialisation {
	declare parents : arbreLexicographique implements Serializable;

	public void ArbreLexicographique.sauve(String nomFichier) {
        String filename = nomFichier + ".txt"; 
      
        // Serialization  
        try
        {    
            //Saving of object in a file 
            FileOutputStream file = new FileOutputStream(filename); 
            ObjectOutputStream out = new ObjectOutputStream(file); 
              
            // Method for serialization of object 
            out.writeObject(arbreLexicographique); 
              
            out.close(); 
            file.close(); 

            System.out.println("Object has been serialized"); 
  
        } 
          
        catch(IOException ex) 
        { 
            System.out.println("IOException is caught"); 
        } 
	}
	
	public void ArbreLexicographique.charge(String nomFichier) {
			
	}
}
