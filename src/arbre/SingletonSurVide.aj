package arbre;

public aspect SingletonSurVide {
	private final NoeudVide instance = new NoeudVide();
	
	private pointcut appelConstructeur() : call(public NoeudVide.new() && !within(SingletonSurVide));
	
	NoeudVide around() : appelConstructeur() {
		return instance;
	}
}
